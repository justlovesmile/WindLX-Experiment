# WindLX-Experiment

#### 介绍
HEU Computer system structure experiment based on windlx

#### 使用说明

1. 使用见[操作说明.txt](https://gitee.com/justlovesmile/WindLX-Experiment/blob/master/%E6%93%8D%E4%BD%9C%E8%AF%B4%E6%98%8E.txt)
2. 原理见[实验报告.md](https://gitee.com/justlovesmile/WindLX-Experiment/blob/master/%E5%AE%9E%E9%AA%8C%E6%8A%A5%E5%91%8A.md)

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
